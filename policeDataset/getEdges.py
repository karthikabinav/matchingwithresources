import sys
import math
import random
from random import randint
import scipy.stats as stats

n=100
m=120
T=1440
K=7

edges=dict()
E=0

f=open("GraphStructure/edges", "w+")


g=open("GraphStructure/LHSvertices", "r")

LHS=dict()
for line in g:
  vals=line.split(",")
  LHS[vals[0]]=dict()
  LHS[vals[0]]["county"]=vals[1]
  LHS[vals[0]]["lat"]=vals[2]
  LHS[vals[0]]["lon"]=vals[3]
  LHS[vals[0]]["cost"]=float(vals[4].split("\n")[0])

g=open("list_of_offense", "r")
offense=dict()
for line in g:
  vals=line.split(",")
  offense[vals[0]]=dict()
  offense[vals[0]]["arrest"]=float(vals[2])
  offense[vals[0]]["search"]=float(vals[3])
  offense[vals[0]]["speed"]=float(vals[4])
  offense[vals[0]]["citation"]=float(vals[5].split("\n")[0])

g=open("GraphStructure/RHSvertices", "r")
RHS=dict()
for line in g:
  vals=line.split(",")
  Dtype=vals[0] + ";" + vals[1] + ";" + vals[2].split("\n")[0]
  RHS[Dtype]=offense[vals[2].split("\n")[0]]


radThr=2
E=0
for v in RHS:
  count=0
  minDist=1000
  minU=-1
  for u in LHS:
    vlat=float(v.split(";")[0])
    vlon=float(v.split(";")[1])
    voffense=v.split(";")[2]
    
    ulat=float(LHS[u]["lat"])
    ulon=float(LHS[u]["lon"])

    dist=math.sqrt((vlat-ulat)**2 + (vlon-ulon)**2)
    if dist<minDist:
      minDist=dist
      minU=u

    if dist<radThr and count<=5:
      count+=1
      edges[E]=dict()
      edges[E]["LHS"]=u
      edges[E]["RHS"]=v
      edges[E]["weight"]=1
      cost=LHS[u]["cost"]
      edges[E]["resources"]=list()
      edges[E]["resources"].append(dist/radThr)#scaled distance travelled
      edges[E]["resources"].append(cost**randint(1,4)/4)#scaled number of officers
      edges[E]["resources"].append(cost*randint(1,3)/3)#scaled number of patrol vehicles
      edges[E]["resources"].append(cost*offense[voffense]["arrest"])#arrest
      edges[E]["resources"].append(cost*offense[voffense]["search"])#search
      edges[E]["resources"].append(cost*offense[voffense]["speed"])#speed
      edges[E]["resources"].append(cost*offense[voffense]["citation"])#citation

      wr=str(E) + "," + str(u) + "," + str(v) + "," + str(edges[E]["weight"]) + ","
      for res in edges[E]["resources"]:
        wr+=str(res) + ","
      wr=wr[:-1]
      wr+="\n"
      f.write(wr)
      E+=1
  
  assert(count>0)
  print count





