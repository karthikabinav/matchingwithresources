import sys
import scipy.stats as stats
from collections import defaultdict
import numpy as np
import cvxopt
from cvxopt import matrix, spmatrix, solvers
import random

K=7 #total number of resources
T=1440 #Total time steps
n=20
m=120

exp=sys.argv[1]

valsl=list()
I=list()
J=list()

edges=dict()

f=open("GraphStructure/edges", "r")

EJT=dict()
for line in f:
  vals=line.split("\n")[0].split(",")
  edges[vals[0]]=dict()
  edges[vals[0]]["LHS"]=vals[1]
  edges[vals[0]]["RHS"]=vals[2]
  edges[vals[0]]["weight"]=float(vals[3])
  edges[vals[0]]["resources"]=list()
  
  if vals[2] not in EJT:
    EJT[vals[2]]=list()
  EJT[vals[2]].append(vals[1])
  for i in xrange(4, len(vals)):
    edges[vals[0]]["resources"].append(float(vals[i].split("\n")[0]))
f.close()

LHS=dict()
f=open("GraphStructure/LHSvertices", "r")
for line in f:
  vals=line.split("\n")[0].split(",")
  LHS[vals[0]]=1
f.close()

RHS=dict()
RHSQuery=dict()
f=open("GraphStructure/RHSvertices", "r")
Jc=0
for line in f:
  vals=line.split("\n")[0].split(",")
  Dtype=vals[0] + ";" + vals[1] + ";" + vals[2].split("\n")[0]
  RHS[Dtype]=dict()
  RHS[Dtype]["arrivals"]=list()
  RHSQuery[Jc]=Dtype
  Jc+=1
f.close()

f=open("GraphStructure/RHSvertices_rates", "r")
for line in f:
    vals=line.split("\n")[0].split(",")
    RHS[vals[0]]["arrivals"].append(float(vals[2]))
f.close()

#Obtain Ejt and write to file
f=open("GraphStructure/Ejt", "w+")
for t in xrange(T):
  for j in EJT:
    wr=str(j) + "," + str(t) + ","
    for i in EJT[j]:
      wr+=str(i) + ","
    wr=wr[:-1]
    wr+="\n"
    f.write(wr)
f.close()

We=list()
E=0
for e in edges:
  We.append(edges[e]["weight"])
  E+=1

W=list() #Weights vector
for t in xrange(T):
  for e in edges:
    task=edges[e]["LHS"]
    worker=edges[e]["RHS"] 
    if task in EJT[worker]:
      W.append(We[int(e)])
    else:
      W.append(0)
b=list()#b vector

f=open("GraphStructure/Bk" + exp, "r")
Bk=list()
for line in f:
  vals=line.split("\n")[0]
  Bk.append(int(vals))

#valsl.append(0)
#I.append(m*T+K)
#J.append(E*T)

#Construct the top half of matrix
for i in xrange(m*T):
  t=i/m
  worker=i%m
  workerType=RHSQuery[worker]
  b.append(RHS[workerType]["arrivals"][t])
  #b.append(1)

  for ed in xrange(E):
    if edges[str(ed)]["RHS"]!=workerType:
      continue
    if edges[str(ed)]["LHS"] in EJT[workerType]:
      valsl.append(1.0)
      I.append(i + 1)
      J.append(t*E + ed + 1)
for k in xrange(K):
  b.append(Bk[k])
  for j in xrange(E * T):
    t=j/E
    ed=j%E
    worker=edges[str(ed)]["RHS"]

    if k in xrange(len(edges[str(ed)]["resources"])):
      if edges[str(ed)]["LHS"] in EJT[worker]:
        valsl.append(float(edges[str(ed)]["resources"][k]))
        I.append(m*T + k + 1)
        J.append(j + 1)
f = open("LP/vals"+exp, "w+")
np.array(valsl).tofile(f, sep=",")
f.close()
f = open("LP/I"+exp, "w+")
np.array(I).tofile(f, sep=",")
f.close()
f = open("LP/J"+exp, "w+")
np.array(J).tofile(f, sep=",")
f.close()
f = open("LP/b"+exp, "w+")
print "b=", len(b)
b=matrix(b)
np.array(b).tofile(f, sep=",")
f.close()
f = open("LP/c"+exp, "w+")
print "W=", len(W)
W=matrix(W)
np.array(W).tofile(f, sep=",")
f.close()



#A=spmatrix(valsl, I, J)
#b=matrix(b)
#W=matrix(W)
#sol=solvers.lp(W, A, b)
#print sol

