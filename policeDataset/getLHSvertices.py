import operator
import numpy as np

f=open("TX_100_small", "r")
g=open("./GraphStructure/LHSvertices", "w+")
first=True
LHSvertices=dict()
countyNames=dict()
lat=dict()
lon=dict()

for line in f:
  if first:
    first=False
    continue
  
  vals=line.split(",")
  
  county=int(vals[6])
  if county not in LHSvertices:
    LHSvertices[county]=1
    countyNames[county]=vals[5]
    try:
      lat[county]=round(float(vals[-4]),0)
      lon[county]=round(float(vals[-3]), 0)
    except:
      pass
  else:
    LHSvertices[county]+=1

    if county not in lat:
      try:
        lat[county]=round(float(vals[-4]),0)
        lon[county]=round(float(vals[-3]), 0)
      except:
        pass


sorted_LHS=sorted(LHSvertices.items(), key=operator.itemgetter(1), reverse=True)

count=0
for key,value in sorted_LHS:
  cost=np.random.uniform(0, 0.8)
  g.write(str(key) + "," + countyNames[key] + "," +  str(lat[key]) + "," + str(lon[key]) + "," + str(cost) + "\n")
  print value
  count+=1

  if count>=20:
    break

g.close()
