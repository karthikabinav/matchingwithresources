root = './LP/';
val = csvread(strcat(strcat(root, 'vals'),expe));
I = csvread(strcat(strcat(root, 'I'), expe));
J = csvread(strcat(strcat(root, 'J'), expe));

b = csvread(strcat(strcat(root, 'b'), expe));
c = csvread(strcat(strcat(root, 'c'), expe));


A = sparse(I,J, val);
size(A)
size(c)
options = optimoptions(@linprog,'Display', 'iter')
[x, fval] = linprog(-c,A,b,[], [], zeros(size(c)), ones(size(c)), [], options);

csvwrite(strcat(strcat(root, 'LPval'), expe), fval);
csvwrite(strcat(strcat(root, 'X'), expe), x);
