import random

T=1440
m=120

RHSf=open("GraphStructure/RHSvertices_rates", "r")
RHSMap=dict()
RHS=[[0 for t in xrange(T)] for j in xrange(m)] 
j=0
for line in RHSf:
  vals=line.split("\n")[0].split(",")
  t=int(vals[1])
  RHS[j][t]=float(vals[2].split("\n")[0])
  RHSMap[j]=vals[0]
  if t==1439:
    j+=1

for i in xrange(1, 11):
  f=open("runs/" + str(i) + "/arrivals", "w+")

  for t in xrange(T):
    r=random.uniform(0,1)
    total=0
    fd=False
    for j in xrange(m):
      if total+RHS[j][t]>=r and total<r:
        f.write(RHSMap[j] + "\n")
        fd=True
        break
      total+=RHS[j][t]
    if not fd:
      f.write(str(-1) + "\n")

