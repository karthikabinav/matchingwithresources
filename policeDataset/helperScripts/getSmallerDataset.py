import csv
import pandas as pd

df=pd.read_csv("./TX_2010_small")

f=open("./TX_2010_small_sampled.csv", "w+")
f.write("id,state,stop_date,stop_time,county_name,county_fips,police_department,\
    driver_gender,driver_age,driver_race,violation,search_conducted,search_type,\
    contraband_found,stop_outcome,is_arrested,lat,lon,officer_id")
counties=dict()
for index,row in df.iterrows():
  if row["county_fips"] not in counties:
    counties[row["county_fips"]]=1
  elif counties[row["county_fips"]]<500:
    counties[row["county_fips"]]=+1
  else:
    continue
  
  f.write(row["id"] + "," + row["state"] + "," + row["stop_date"] + "," + row["stop_time"] \
      +"," + row["county_name"] + "," + row["county_fips"] + "," + row["police_department"] \
      +"," + row["driver_gender"] + "," + row["driver_age"] + "," + row["driver_race"] + "," \
      + row["violation"] + "," + + row["search_conducted"] + "," + row["search_type"] + "," \
      + row["contraband_found"] + "," + row["stop_outcome"] + "," + row["is_arrested"] + "," \
      + row["lat"] + "," + row["long"] + "," + row["officer_id"] + "\n")

f.close()


