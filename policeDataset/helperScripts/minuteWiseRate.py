import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt


f=open("TX_2010_48215", "r")

total=dict()
count=dict()
for i in xrange(1440):
  total[i]=0

for line in f:
  vals=line.split(",")
  time=vals[3]
  
  date=vals[2]
  step=int(time.split(":")[0])*60+int(time.split(":")[1])
  total[step]+=1

  if date not in count:
    count[date]=1

X=list()
Y=list()

for key in total:
  total[key]=(1.0*total[key])/float(len(count.keys()))
  X.append(key)
  Y.append(total[key])

#Plot the total arrival rates
  
plt.plot(X, Y)
plt.xlabel("Time-step")
plt.ylabel("Arrival Rate")
plt.savefig("TimeWise_48215.png")

