f=open("TX_100_learn", "r")

g=open("GraphStructure/RHSvertices", "r")
RHSvertex=dict()

for line in g:
  vals=line.split(",")
  Dtype=vals[0] + ";" + vals[1] + ";" + vals[2].split("\n")[0]
  RHSvertex[Dtype]=dict()

  for i in xrange(1440):
    RHSvertex[Dtype][i]=1

g=open("GraphStructure/RHSvertices_rates", "w+")

first=True
date=dict()
for line in f:
  if first:
    first=False
    continue
    
  vals=line.split(",")
  try:
    lat=round(float(vals[-4]),0)
    lon=round(float(vals[-3]),0)

    violation=vals[-11].split("\"")[0]
    if violation[0]=='O':
      assert(1==2)
  
    Dtype=str(lat) +";" + str(lon) + ";" + violation
      
    rec_date=vals[2]
    if rec_date not in date:
      date[rec_date]=1
    if Dtype in RHSvertex:
      time=vals[3]
      hour=int(time.split(":")[0])
      minute=int(time.split(":")[1])
      step=hour*60+minute
      RHSvertex[Dtype][step]+=1
  except:
    pass


for i in xrange(1440):
  total=0
  for d in RHSvertex:
    total+=RHSvertex[d][i]
  
  for d in RHSvertex:
    RHSvertex[d][i]/=float(total)

for d in RHSvertex:
  for i in xrange(1440):
    g.write(d + "," + str(i) + "," + str(RHSvertex[d][i]) + "\n")
g.close()

