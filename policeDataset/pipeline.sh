#(1) get the LHS vertices
python getLHSvertices.py
#(2) Get RHS vertices
python getRHSvertices.py
#(3) Get Edges
python getEdges.py
#(4) Learn the arrival rates
python getArrivalRates.py
#(5) Get Arrival sequences for the experiments
python getArrivals.py
#(6) Get the Sparse Matrices for LP
python getSparseA.py Random
#(7) Solve the LP
bash run_code Random
