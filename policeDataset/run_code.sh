#!/bin/bash

#vals=(2 4 5 6 8)
#for i in "${vals[@]}"
#do
  i=100
  python getSparseA.py $i
  module load matlab/2016b
  matlab -nodisplay -nosplash -nodesktop -r "expe='$i';run('./lp_solver.m');exit;"
  echo "Done $i"
#done
