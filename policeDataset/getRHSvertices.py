import matplotlib
import operator
import math
matplotlib.use('agg')
import matplotlib.pyplot as plt

f=open("TX_100_small", "r")

first=True
RHSvertex=dict()
num_arrs=dict()
is_arrest=dict()

for line in f:
  if first:
    first=False
    continue
  
  vals=line.split(",")
  
  try:
    lat=round(float(vals[-4]),0)
    lon=round(float(vals[-3]),0)

    violation=vals[-11].split("\"")[0]
    if violation[0]=='O':
      assert(1==2)
  
    Dtype=str(lat) +";" + str(lon) + ";" + violation

    if Dtype not in RHSvertex:
      RHSvertex[Dtype]=1
      #g.write(str(lat) + "," + str(lon) + "," + violation + "\n")
      num_arrs[Dtype]=1
    else:
      num_arrs[Dtype]+=1
  except:
    pass

#Filter top closest crimes to the current LHS vertices
g=open("GraphStructure/LHSvertices", "r")
LHSvertices=list()

for line in g:
  vals=line.split(",")
  LHSvertices.append((float(vals[2]), float(vals[3].split("\n")[0])))

RHSvertexSmall=dict()
for d in RHSvertex:
  vlat=float(d.split(";")[0])
  vlon=float(d.split(";")[1])
  
  found=0
  for l in LHSvertices:
    dist=math.sqrt((l[0]-vlat)**2 + (l[1]-vlon)**2)
    if dist<2:
      found+=1
  
  if found>0:
    RHSvertexSmall[d]=found

g=open("GraphStructure/RHSvertices", "w+")

sorted_RHS=sorted(RHSvertexSmall.items(), key=operator.itemgetter(1), reverse=True) #half of them very close
count=0
for d, value in sorted_RHS:
  lat=d.split(";")[0]
  lon=d.split(";")[1]
  violation=d.split(";")[2].split("\n")[0]

  g.write(str(lat) + "," + str(lon) + "," + violation + "\n")

  count+=1

  if count>=60:
    break

sorted_RHS=sorted(RHSvertexSmall.items(), key=operator.itemgetter(1))#half of them really far
count=0
for d, value in sorted_RHS:
  lat=d.split(";")[0]
  lon=d.split(";")[1]
  violation=d.split(";")[2].split("\n")[0]

  g.write(str(lat) + "," + str(lon) + "," + violation + "\n")

  count+=1

  if count>=60:
    break


g.close()

#Plot the total arrival rates
X=list()
Y=list()
count=1
for key in num_arrs:
  X.append(count)
  count+=1
  Y.append(num_arrs[key])
  
plt.plot(X, Y)
plt.xlabel("Online type")
plt.ylabel("Total number of arrivals")
plt.savefig("TotalArrivals.png")
