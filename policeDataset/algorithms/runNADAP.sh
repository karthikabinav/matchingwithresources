#!/bin/bash

for i in `seq 1 10`;
do
  python NAdap.py $1 $i >> outNADAP_"$1" 
done

awk '{s+=$1}END{print "ave:",s/NR}' RS="\n" outNADAP_"$1"

