#!/bin/bash

for i in `seq 1 10`;
do
  python Greedy.py $1 $i >> outGreedy_"$1" 
done

awk '{s+=$1}END{print "ave:",s/NR}' RS="\n" outGreedy_"$1"

