import sys
import random
from collections import Counter

exp=sys.argv[1]
run=sys.argv[2]

ROOT="../"

T=1440
n=20
m=120
K=8
alpha=1

edges=dict()
edgeQuery=dict()
f=open(ROOT + "GraphStructure/edges", "r")
E=0
for line in f:
  vals=line.split("\n")[0].split(",")
  edges[vals[0]]=dict()
  edges[vals[0]]["LHS"]=vals[1]
  edges[vals[0]]["RHS"]=vals[2]
  edges[vals[0]]["weight"]=float(vals[3])
  edges[vals[0]]["resources"]=list()
  edges[vals[0]]["xe"]=list()

  edgeQuery[vals[1] + ";" + vals[2]]=vals[0]
  E+=1
  for i in xrange(4, len(vals)):
    edges[vals[0]]["resources"].append((i-4, float(vals[i].split("\n")[0])))
f.close()

f=open(ROOT + "LP/X" + exp, "r")
e=0
for line in f:
  val=line.split("\n")[0]
  edges[str(e%E)]["xe"].append(float(val)) 
  e+=1

LHS=dict()
f=open(ROOT + "GraphStructure/LHSvertices", "r")
for line in f:
  vals=line.split("\n")[0].split(",")
  LHS[vals[0]]=dict()
  LHS[vals[0]]["active"]=True
f.close()
  
RHS=dict()
f=open(ROOT + "GraphStructure/RHSvertices", "r")
for line in f:
  vals=line.split("\n")[0].split(",")
  Dtype=vals[0] + ";" + vals[1] + ";" + vals[2].split("\n")[0]
  RHS[Dtype]=dict()
  RHS[Dtype]["arrivals"]=list()
f.close()

f=open(ROOT + "GraphStructure/RHSvertices_rates", "r")
for line in f:
  vals=line.split(",")
  RHS[vals[0]]["arrivals"].append(float(vals[2].split("\n")[0]))

EJT=dict()
f=open(ROOT + "GraphStructure/Ejt", "r")

for line in f:
  vals=line.split("\n")[0].split(",")
  
  if vals[0] not in EJT:
    EJT[vals[0]]=dict()
    for t in xrange(T):
      EJT[vals[0]][t]=list()

  for i in xrange(2, len(vals)):
    EJT[vals[0]][int(vals[1])].append(vals[i])

Bk=dict()
f=open(ROOT + "GraphStructure/Bk" + exp , "r")
k=0
for line in f:
  val=line.split("\n")[0]
  Bk[k]=int(val)
  k+=1

RHSarrivals=[0 for t in xrange(T)]
f=open(ROOT + "runs/" + run + "/arrivals", "r" )
t=0
for line in f:
  vals=line.split("\n")[0]
  RHSarrivals[t]=vals
  t+=1

#Run the actual algorithm
ALGcost=0
LPvalue=0

#Obtain the cost of LP
LPf=open(ROOT + "LP/LPval" + exp, "r")
for line in LPf:
  LPvalue=-1*float(line.split("\n")[0])


count = 0
for t in xrange(T):
  RHSarrival = RHSarrivals[t]
  if RHSarrival == str(-1):
    continue
  
  #Get Available Assignments
  available=list()
  for key in EJT[RHSarrival][t]:
    potentialEdge=str(key) + ";" + str(RHSarrival)
    possible=True
    ed=edgeQuery[potentialEdge]
    for i,val in edges[ed]["resources"]:
      if Bk[i]<val:
        possible=False
        break
    if possible:
      available.append(key)
  #Make an assignment
  assigned = -1
  if len(available)>0:
    assigned = random.choice(available)

  if assigned == -1:
    continue
  matchedEdge=str(assigned) + ";" + str(RHSarrival)
  ed=edgeQuery[matchedEdge]
  for i,val in edges[ed]["resources"]:
    Bk[i]-=val

  ALGcost+=edges[ed]["weight"]

print float(ALGcost)/float(LPvalue)
#print float(ALGcost)
#print float(LPvalue)

