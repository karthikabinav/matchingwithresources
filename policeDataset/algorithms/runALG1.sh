#!/bin/bash

for i in `seq 1 10`;
do
  python ALG1.py $1 $i >> outALG1_"$1" 
done

awk '{s+=$1}END{print "ave:",s/NR}' RS="\n" outALG1_"$1"

