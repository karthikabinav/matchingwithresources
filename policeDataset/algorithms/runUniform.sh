#!/bin/bash

for i in `seq 1 10`;
do
  python Uniform.py $1 $i >> outUniform_"$1" 
done

awk '{s+=$1}END{print "ave:",s/NR}' RS="\n" outUniform_"$1"

