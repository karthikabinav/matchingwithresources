
###### In folder setup_codes #####
#(1) get the LHS vertices
python getLHSvertices.py
#(2) Get RHS vertices
python getRHSvertices.py
#(3) Sample Jobs
python sampleJobs.py n total
#(4) Sample Machines
python sampleMachines.py m total
#(5) Get Arrival Rates
python getArrivalRates.py n
#(6) Get Graph
python getGraph.py m n
#(7) Get Arrival sequences for the experiments
python getArrivals.py n samples

######## In folder LP_codes ########
#(8) Get the Sparse Matrices for LP
python getSparseA.py n m B T
#(7) Solve the LP
bash run_code Random
