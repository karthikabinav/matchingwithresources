#!/bin/bash

m=$1
n=$2
T=$3
folder=$4

for i in `seq 0 2`;
do
  python "$folder"/Uniform.py $i $m $n $T >> outUniform_"$m"_"$n"_"$T"_"$folder" 
  echo $i
done

awk '{s+=$1}END{print "ave:",s/NR}' RS="\n" outUniform_"$m"_"$n"_"$T"_"$folder"

