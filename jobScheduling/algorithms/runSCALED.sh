#!/bin/bash

m=$1
n=$2
T=$3
folder=$4

for i in `seq 0 2`;
do
  python "$folder"/SCALED.py $i $m $n $T >> outSCALED_"$m"_"$n"_"$T"_"$folder"
  echo $i
done

awk '{s+=$1}END{print "ave:",s/NR}' RS="\n" outSCALED_"$m"_"$n"_"$T"_"$folder"

