import sys
import random
from collections import Counter
 
run=sys.argv[1]
m=int(sys.argv[2])
n=int(sys.argv[3])
T=int(sys.argv[4])
exp=str(m) + "_" + str(n) + "_" + str(T)

K=2*m

ROOT="../"

alpha=1

edges=dict()
edgeQuery=dict()
f=open(ROOT + "GraphStructure/graph_" + str(m) + "_" + str(n) + ".csv", "r")

EJT=dict()
E=0

Bk=dict()
for line in f:
  vals=line.split("\n")[0].split(",")
  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["weight"]=float(vals[3])
  edges[vals[0]]["resources"]=list()
  edges[vals[0]]["xe"]=list()
  #RHS + LHS
  edgeQuery[vals[2] + ";" + vals[1]]=vals[0]
  E+=1
  
  res=vals[4].split(";")
  
  #Create a list of size of number of resources. Then put all zeros except the two places where this resource is consumed.
  edges[vals[0]]["resources"]=[0 for i in xrange(K)]
  edges[vals[0]]["resources"][int(res[0])] = float(vals[1].split(";")[1])
  edges[vals[0]]["resources"][int(res[1])] = float(vals[1].split(";")[2])
  Bk[int(res[0])]=float(vals[2].split(";")[1])
  Bk[int(res[1])]=float(vals[2].split(";")[2])

f.close()

LHS=dict()
f=open(ROOT + "GraphStructure/machines_" + str(m) + ".csv", "r")
for line in f:
  vals=line.split("\n")[0].split(",")
  _lhs=vals[0] + ";" + vals[1] + ";" + vals[2]
  LHS[_lhs]=dict()
  LHS[_lhs]["active"]=True
f.close()

RHS=dict()
RHSQuery=dict()
RHSMatched=dict()
RHSAppeared=dict()

f=open(ROOT + "GraphStructure/jobsWithArrivalRates_" + str(n) + ".csv", "r")
Jc=0
for line in f:
  vals=line.split("\n")[0].split(",")
  Dtype=vals[0] + ";" + vals[2] + ";" + vals[3] 
  RHS[Dtype]=dict()
  RHSQuery[Jc]=Dtype
  Jc+=1
  RHS[Dtype]["arrivals"]=list()

  RHSMatched[Dtype]=0
  RHSAppeared[Dtype]=0


  for i in xrange(T):
    RHS[Dtype]["arrivals"].append(float(vals[4]))
f.close()

EJT=dict()
f=open(ROOT + "GraphStructure/Ejt", "r")

for line in f:
  vals=line.split("\n")[0].split(",")
  
  if vals[0] not in EJT:
    EJT[vals[0]]=dict()
    for t in xrange(T):
      EJT[vals[0]][t]=list()

  for i in xrange(2, len(vals)):
    EJT[vals[0]][int(vals[1])].append(vals[i])

RHSarrivals=[0 for t in xrange(T)]
f=open(ROOT + "Arrivals/" + str(n) + "/" + run + ".csv", "r" )
t=0
for line in f:
  vals=line.split("\n")[0]
  RHSarrivals[t]=vals
  t+=1

  if t>=T:
    break


#Run the actual algorithm
ALGcost=0
LPvalue=0

#Obtain the cost of LP
LPf=open(ROOT + "LP/LPval" + exp, "r")
for line in LPf:
  LPvalue=-1*float(line.split("\n")[0])

count = 0
for t in xrange(T):
  RHSarrival = RHSarrivals[t]
  if RHSarrival == str(-1):
    continue
  
  RHSAppeared[str(RHSarrival)]+=1
  #Get Available Assignments
  available=list()
  for key in EJT[RHSarrival][t]:
    potentialEdge=str(key) + ";" + str(RHSarrival)
    possible=True
    ed=edgeQuery[potentialEdge]
    for i in xrange(len(edges[ed]["resources"])):
      val=edges[ed]["resources"][i]
      if Bk[i]<val:
        possible=False
        break
    if possible:
      available.append(key)
  #Make an assignment
  assigned = -1
  maxi = -1

  for u in available:
    edge = str(u) + ";" + str(RHSarrival)
    ed=edgeQuery[edge]
    if edges[ed]["weight"] > maxi:
      maxi = edges[ed]["weight"]
      assigned=u
  if assigned == -1:
    continue
  matchedEdge=str(assigned) + ";" + str(RHSarrival)
  RHSMatched[str(RHSarrival)]+=1
  ed=edgeQuery[matchedEdge]
  for i in xrange(len(edges[ed]["resources"])):
    val=edges[ed]["resources"][i]
    Bk[i]-=val

  ALGcost+=edges[ed]["weight"]

#Compute the MaxSum
_sum=0
for Dtype in RHSMatched:
  _sum+=RHSAppeared[Dtype]-RHSMatched[Dtype]
  
print _sum









