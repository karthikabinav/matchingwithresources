import sys
import numpy as np

n=int(sys.argv[1])
total=int(sys.argv[2])

f=open("../GraphStructure/jobs.csv", "r")
g=open("../GraphStructure/jobs_" + str(n) + ".csv", "w+")

entries=np.random.choice(total, n, replace=False)

_entry=0
for line in f:
  if _entry not in entries:
    _entry+=1
    continue
  
  g.write(line)
  _entry+=1

f.close()
g.close()
  
  
