import sys
import numpy as np

m=int(sys.argv[1])
total=int(sys.argv[2])

f=open("../GraphStructure/machines.csv", "r")
g=open("../GraphStructure/machines_" + str(m) + ".csv", "w+")

entries=np.random.choice(total, m, replace=False)

_entry=0
for line in f:
  if _entry not in entries:
    _entry+=1
    continue
  
  g.write(line)
  _entry+=1

f.close()
g.close()
  
  
