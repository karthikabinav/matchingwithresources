import numpy as np
import sys

ROOT="../../../CloudDatasetSmall/"
_tasks_file=sys.argv[1]
_output_file=sys.argv[2]


f=open(ROOT + _tasks_file, "r")
g=open(ROOT + _output_file, "w+")

tasks=dict()

for line in f:
  try:
    vals=line.split(",")
    _time=int(vals[0])
    _missing=vals[1]
    _id=int(vals[2])
    _task_index=vals[3]
    _machine_id=int(vals[4])
    _event_type=vals[5]
    user_name=vals[6]
    sched=vals[7]
    priority=vals[8]
    _CPU=float(vals[9])
    _mem=float(vals[10])
    loc=vals[11]
    cons=vals[12]
  except:
    continue

  if _id in tasks:
    continue

  tasks[_id]=1
    
  #Schema: task_id, _time, CPU, memory
  g.write(str(_id) + "," + str(_time) + "," + str(_CPU) + "," + str(_mem) + "\n")


g.close()
f.close()
  

