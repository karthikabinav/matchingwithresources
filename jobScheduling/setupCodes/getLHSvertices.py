import numpy as np
import sys

ROOT="../../../CloudDatasetSmall/"
_server_file=sys.argv[1]
_output_file=sys.argv[2]


f=open(ROOT + _server_file, "r")
g=open(ROOT + _output_file, "w+")

machines=dict()

for line in f:
  try:
    vals=line.split(",")
    _time=int(vals[0])
    _id=int(vals[1])
    _event_type=int(vals[2])
    _plat_id=vals[3]
    _CPU=float(vals[4])
    _mem=float(vals[5])
  except:
    continue

  if _id in machines:
    continue
  if _event_type!=0:
    continue

  machines[_id]=1
    
  #Schema: machine_id, CPU, memory
  g.write(str(_id) + "," + str(_CPU) + "," + str(_mem) + "\n")


g.close()
f.close()
  

