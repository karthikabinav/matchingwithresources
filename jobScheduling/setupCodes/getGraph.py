import sys
import numpy as np

m=int(sys.argv[1])
n=int(sys.argv[2])

f_LHS=open("../GraphStructure/machines_" + str(m) + ".csv", "r")
f_RHS=open("../GraphStructure/jobs_" + str(n) + ".csv", "r")

g=open("../GraphStructure/graph_" + str(m) + "_" + str(n) + ".csv", "w+")

LHS=dict()

_lhs_id=0
for line in f_LHS:
  vals=line.split("\n")[0].split(",")
  _id=vals[0]
  _CPU=vals[1]
  _mem=vals[2]

  LHS[_lhs_id]=_id + ";" + _CPU + ";" + _mem
  _lhs_id+=1

_edge=0
_res=0
LHS_res=dict()
for line in f_RHS:
  _neigh=np.random.choice(m, 10, replace=False)
  
  vals=line.split("\n")[0].split(",")
  _id=vals[0]
  _time=vals[1]
  _CPU=vals[2]
  _mem=vals[3]
  
  _rhs= _id + ";" + _CPU + ";" + _mem

  for _neigh_id in _neigh:
    #Schema: Edge number, RHS vertex, LHS vertex
    weight=np.random.uniform(0, 1)
    
    _lhs=LHS[_neigh_id]
    if _lhs not in LHS_res:
      LHS_res[_lhs]=str(_res) + ";" + str(_res+1)
      _res+=2

    resources=LHS_res[_lhs]
    g.write(str(_edge) + "," + _rhs + "," + LHS[_neigh_id] + "," + str(weight) + "," + resources + "\n")
    _edge+=1

g.close()
f_RHS.close()
f_LHS.close()
  
  
  






