import sys
import numpy as np

n=int(sys.argv[1])
samples=int(sys.argv[2])

f=open("../GraphStructure/jobsWithArrivalRates_" + str(n) + ".csv", "r")

RHS=dict()
_pos=0

prob=[list() for t in xrange(100*n)]
for line in f:
  vals=line.split("\n")[0].split(",")
  _id=vals[0]
  _CPU=vals[2]
  _mem=vals[3]

  for i in xrange(4, len(vals)):
    _prob=vals[i]
    prob[i-4].append(_prob)

  _rhs=_id + ";" + _CPU + ";" + _mem
  RHS[_pos]=_rhs
  _pos+=1

f.close()

for _samp in xrange(samples):
  g=open("../Arrivals/" + str(n) +"/" + str(_samp) + ".csv", "w+")
  
  arrivals=list()
  for t in xrange(100*n):
    arr=np.random.choice(n, 1, p=prob[t])
    arrivals.append(arr[0])
  
  for _arr in arrivals:
    g.write(RHS[_arr] + "\n")

  g.close()



