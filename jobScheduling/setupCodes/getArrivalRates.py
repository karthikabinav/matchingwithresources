import sys
import numpy as np


def getRandomDist(dim):
  X=abs(np.random.normal(0, 1, dim))
  X=X/np.linalg.norm(X, 1)
  
  return X

n=int(sys.argv[1])

f=open("../GraphStructure/jobs_" + str(n) + ".csv", "r")
g=open("../GraphStructure/jobsWithArrivalRates_" + str(n) + ".csv", "w+")


Prob=[list() for i in xrange(n)]

for t in xrange(100*n):
  P=getRandomDist(n)
  for i in xrange(n):
      Prob[i].append(P[i])

_i=0
for line in f:
  vals=line.split("\n")[0]
  
  prob=""
  for t in xrange(100*n):
    prob = prob + str(Prob[_i][t]) +","

  prob=prob[:-1]
  g.write(vals + "," + prob + "\n")
  _i+=1
  print _i

f.close()
g.close()



