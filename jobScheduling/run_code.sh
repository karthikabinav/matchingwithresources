#!/bin/bash

#vals=(2 4 5 6 8)
#for i in "${vals[@]}"
#do
  #i=100
  #python getSparseA.py $i
echo $1
m=$1
n=$2
T=$3
  module load matlab/2016b
  i="$m"_"$n"_"$T"
  echo $i
  matlab -nodisplay -nosplash -nodesktop -r "expe='$i';run('./LP_codes/lp_solver.m');exit;"
  echo "Done $i"
#done
