import sys
import scipy.stats as stats
from collections import defaultdict
import numpy as np
import cvxopt
from cvxopt import matrix, spmatrix, solvers
import random

m=int(sys.argv[1])
n=int(sys.argv[2])
T=int(sys.argv[3])

K=2*m #total number of resources

exp=str(m) + "_" + str(n) + "_" +str(T)

valsl=list()
I=list()
J=list()
Bk=[0 for i in xrange(K)]

edges=dict()

f=open("../GraphStructure/graph_" + str(m) + "_" + str(n) + ".csv", "r")

EJT=dict()

for line in f:
  vals=line.split("\n")[0].split(",")
  edges[vals[0]]=dict()
  edges[vals[0]]["RHS"]=vals[1]
  edges[vals[0]]["LHS"]=vals[2]
  edges[vals[0]]["weight"]=float(vals[3])
  edges[vals[0]]["resources"]=list()
  
  if vals[1] not in EJT:
    EJT[vals[1]]=list()
  EJT[vals[1]].append(vals[2])

  res=vals[4].split(";")
  
  #print int(res[0]), int(res[1])
  #Create a list of size of number of resources. Then put all zeros except the two places where this resource is consumed.
  edges[vals[0]]["resources"]=[0 for i in xrange(K)]
  edges[vals[0]]["resources"][int(res[0])] = float(vals[1].split(";")[1])
  edges[vals[0]]["resources"][int(res[1])] = float(vals[1].split(";")[2])

  Bk[int(res[0])]=float(vals[2].split(";")[1])
  Bk[int(res[1])]=float(vals[2].split(";")[2])
f.close()

LHS=dict()
f=open("../GraphStructure/machines_" + str(m) + ".csv", "r")
for line in f:
  vals=line.split("\n")[0].split(",")
  _lhs=vals[0] + ";" + vals[1] + ";" + vals[2]
  LHS[_lhs]=1
f.close()

RHS=dict()
RHSQuery=dict()
f=open("../GraphStructure/jobsWithArrivalRates_" + str(n) + ".csv", "r")
Jc=0
for line in f:
  vals=line.split("\n")[0].split(",")
  Dtype=vals[0] + ";" + vals[2] + ";" + vals[3] 
  RHS[Dtype]=dict()
  RHSQuery[Jc]=Dtype
  Jc+=1
  RHS[Dtype]["arrivals"]=list()

  for i in xrange(T):
    RHS[Dtype]["arrivals"].append(float(vals[i+4]))
f.close()

#Obtain Ejt and write to file
f=open("../GraphStructure/Ejt", "w+")
for t in xrange(T):
  for j in EJT:
    wr=str(j) + "," + str(t) + ","
    for i in EJT[j]:
      wr+=str(i) + ","
    wr=wr[:-1]
    wr+="\n"
    f.write(wr)
f.close()

We=list()
E=0
for e in edges:
  We.append(edges[e]["weight"])
  E+=1


W=list() #Weights vector
for t in xrange(T):
  for e in edges:
    task=edges[e]["LHS"]
    worker=edges[e]["RHS"] 
    if task in EJT[worker]:
      W.append(We[int(e)])
      #W.append(1.0)
    else:
      W.append(0)# Just redundant. Since EJT is same for every time-step

b=list()#b vector

#Construct the top half of matrix
for i in xrange(n*T):
  t=i/n
  worker=i%n
  workerType=RHSQuery[worker]
  b.append(RHS[workerType]["arrivals"][t])
  #b.append(1)
  
  for e in edges:
    if edges[e]["RHS"]!=workerType:
      continue
    if edges[e]["LHS"] in EJT[workerType]:
      valsl.append(1.0)
      I.append(i + 1)
      J.append(t*E + int(e) + 1)

for k in xrange(K):
  b.append(Bk[k])
  for j in xrange(E * T):
    t=j/E
    ed=j%E
    worker=edges[str(ed)]["RHS"]

    if k in xrange(len(edges[str(ed)]["resources"])):
      if edges[str(ed)]["LHS"] in EJT[worker] and float(edges[str(ed)]["resources"][k])>0.001:
        valsl.append(float(edges[str(ed)]["resources"][k]))
        I.append(n*T + k + 1)
        J.append(j + 1)
f = open("../LP/vals"+exp, "w+")
np.array(valsl).tofile(f, sep=",")
f.close()
f = open("../LP/I"+exp, "w+")
np.array(I).tofile(f, sep=",")
f.close()
f = open("../LP/J"+exp, "w+")
np.array(J).tofile(f, sep=",")
f.close()
f = open("../LP/b"+exp, "w+")
#print "b=", len(b)
b=matrix(b)
np.array(b).tofile(f, sep=",")
f.close()
f = open("../LP/c"+exp, "w+")
#print "W=", len(W)
W=matrix(W)
np.array(W).tofile(f, sep=",")
f.close()



#A=spmatrix(valsl, I, J)
#b=matrix(b)
#W=matrix(W)
#sol=solvers.lp(W, A, b)
#print sol

